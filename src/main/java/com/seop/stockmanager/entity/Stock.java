package com.seop.stockmanager.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String barcodeNum; // 바코드번호
    @Column(nullable = false, length = 20)
    private String productName; // 상품이름
    @Column(nullable = false)
    private Integer productPrice; // 상품가격
    @Column(nullable = false)
    private Integer expirationDate; // 유통기한
    @Column(nullable = false)
    private LocalDate receiveDate; // 입고일
    @Column(nullable = false)
    private LocalDate disposalDate; // 폐기일
    @Column(nullable = false)
    private LocalDate manufactureDate; // 제조일
    @Column(nullable = false)
    private Integer productTotal; //재고량

}
