package com.seop.stockmanager.repository;

import com.seop.stockmanager.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Long> {
}
