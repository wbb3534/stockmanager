package com.seop.stockmanager.controller;

import com.seop.stockmanager.model.*;
import com.seop.stockmanager.service.StockService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/stock")
public class StockController {
    private final StockService stockService;

    @PostMapping("/data")
    public String setStock(@RequestBody @Valid StockRequest request) {
        stockService.setStock(request);
        return "ok";
    }
    @GetMapping("/all")
    public List<StockItem> getStocks() {
        List<StockItem> result = stockService.getStocks();
        return result;
    }
    @GetMapping("/stock/id/{id}")
    public StockResponse getStock(@PathVariable long id) {
        StockResponse result = stockService.getStock(id);
        return result;
    }
    @PutMapping("/price/id/{id}")
    public String putStockPrice(@PathVariable long id, @RequestBody @Valid StockPriceUpdateRequest request) {
        stockService.putStockPrice(id, request);

        return "ok";
    }
    @PutMapping("/total-count/id/{id}")
    public String putStockTotalCount(@PathVariable long id, @RequestBody @Valid StockTotalCountUpdateRequest request) {
        stockService.putStockTotalCount(id, request);

        return "ok";
    }
    @DeleteMapping("/withdrawal/id/{id}")
    public String delStock(@PathVariable long id) {
        stockService.delStock(id);
        return "ok";
    }
}
