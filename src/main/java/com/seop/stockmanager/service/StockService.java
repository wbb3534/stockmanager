package com.seop.stockmanager.service;

import com.seop.stockmanager.entity.Stock;
import com.seop.stockmanager.model.*;
import com.seop.stockmanager.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StockService {
    private final StockRepository stockRepository;

    public void setStock(StockRequest request) {
        Stock addData= new Stock();
        addData.setBarcodeNum(request.getBarcodeNum());
        addData.setProductName(request.getProductName());
        addData.setProductPrice(request.getProductPrice());
        addData.setExpirationDate(request.getExpirationDate());
        addData.setManufactureDate(request.getManufactureDate());
        addData.setProductTotal(request.getProductTotal());
        addData.setReceiveDate(LocalDate.now());
        addData.setDisposalDate(request.getManufactureDate().plusMonths(request.getExpirationDate()));

        stockRepository.save(addData);
    }

    public List<StockItem> getStocks() {
        List<Stock> originData = stockRepository.findAll();
        List<StockItem> result = new LinkedList<>();

        for (Stock item : originData) {
            StockItem addItem = new StockItem();
            addItem.setId(item.getId());
            addItem.setProductNamePrice(item.getProductName() + "/" + item.getProductPrice() + "원");
            addItem.setProductTotal(item.getProductTotal() + "ea");

            String stateName = "";
            if (item.getDisposalDate().isBefore(LocalDate.now())) {
                stateName = "폐기";
            } else if (item.getDisposalDate().minusDays(1).equals(LocalDate.now())) {
                stateName = "임박";
            } else {
                stateName = "정상";
            }

            addItem.setProductStateDate(stateName + "/" + item.getDisposalDate());
            result.add(addItem);
        }
        return result;
    }

    public StockResponse getStock(long id) {
        Stock origin = stockRepository.findById(id).orElseThrow();

        StockResponse result = new StockResponse();
        result.setBarcodeNum(origin.getBarcodeNum());
        result.setProductNamePrice(origin.getProductName() + "/" + origin.getProductPrice());
        result.setExpirationDate(origin.getExpirationDate());
        result.setManufactureDate(origin.getManufactureDate());
        result.setReceiveDisposalDate(origin.getReceiveDate() + "~" + origin.getExpirationDate());

        String stataName = "";
        if (origin.getProductTotal() > 15) {
            stataName = "재고 여유";
        } else if (origin.getProductTotal() > 10) {
            stataName = "재주문 필요";
        } else {
            stataName = "재주문 시급";
        }
        result.setProductTotal(origin.getProductTotal() + "EA" + "/" + stataName);

        return result;
    }

    public void putStockPrice(long id, StockPriceUpdateRequest request) {
        Stock origin = stockRepository.findById(id).orElseThrow();
        origin.setProductPrice(request.getProductPrice());

        stockRepository.save(origin);
    }

    public void putStockTotalCount(long id, StockTotalCountUpdateRequest request) {
        Stock origin = stockRepository.findById(id).orElseThrow();

        origin.setProductTotal(request.getProductTotal());

        stockRepository.save(origin);
    }

    public void delStock(long id) {
        stockRepository.deleteById(id);
    }
}
