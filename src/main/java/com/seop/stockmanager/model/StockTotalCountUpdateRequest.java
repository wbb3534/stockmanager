package com.seop.stockmanager.model;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StockTotalCountUpdateRequest {
    @NotNull
    private Integer productTotal;
}
