package com.seop.stockmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StockResponse {
    private String barcodeNum;
    private String productNamePrice;
    private Integer expirationDate;
    private LocalDate manufactureDate;
    private String receiveDisposalDate;
    private String productTotal;
}
