package com.seop.stockmanager.model;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class StockRequest {
    @NotNull
    @Length(min = 8, max = 20)
    private String barcodeNum;
    @NotNull
    @Length(min = 1, max = 30)
    private String productName;
    @NotNull
    private Integer productPrice;
    @NotNull
    private Integer expirationDate;
    @NotNull
    private LocalDate manufactureDate;
    @NotNull
    private Integer productTotal;

}
