package com.seop.stockmanager.model;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StockPriceUpdateRequest {
    @NotNull
    private Integer productPrice;
}
