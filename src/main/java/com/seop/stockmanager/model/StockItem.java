package com.seop.stockmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StockItem {
    private Long id;
    private String productNamePrice;  // 이름 가격
    private String productTotal;  //  수량
    private String productStateDate; // 물건상태 폐기일
}
